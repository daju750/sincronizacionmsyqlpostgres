package sincronizacion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

public class sincro {
    
    String sql_persona="select * from persona ";
    String replicidad="0";
    String classfor = "org.postgresql.Driver";

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    abb arbol= new abb();
    ac_postgres ac_postgres = new ac_postgres();
    ac_mysql ac_mysql = new ac_mysql();
    
    
    private final Connection con = null;
    private final PreparedStatement pr=null;
    
    
    public Statement cone_postges() throws SQLException{
            DriverManager.registerDriver(new org.postgresql.Driver());
            String url = "jdbc:postgresql://localhost:5432/proyecto1";
            Connection conexion = DriverManager.getConnection (url, "daju300", "wester");
            java.sql.Statement stmt = conexion.createStatement();
    return stmt;
    }
    
    public Statement cone_mysql() throws SQLException{
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            String urlm = "jdbc:mysql://localhost:3306/proyecto1";
            Connection conexionm = DriverManager.getConnection (urlm, "root", "");
             java.sql.Statement stmtm = conexionm.createStatement();
    return stmtm;
    }
    
    
    
    public void sincronizar() throws SQLException
    {
        try{   
            ///////////////////////POSTRESQL/////////////////////////
            java.sql.Statement stmt = cone_postges();
            /////////////////////////MYSQL//////////////////////////
            java.sql.Statement stmtm = cone_mysql();
            
            ResultSet rs=stmt.executeQuery(sql_persona);//postgresl
            while(rs.next()){ String sql=sql_persona+"where dpi in('"+rs.getString("dpi")+"')";
 
                ResultSet rsm=stmtm.executeQuery(sql);//mysql
                while(rsm.next())
                {
                    String v=rs.getString("dpi");
                    arbol.insertar(v);
                    
                    CharSequence post_fecha=rs.getString("fecha").subSequence(0, 19);//postgesql
                    CharSequence mysq_fecha=rsm.getString("fecha").subSequence(0, 19);//mysq
                    //System.out.println("1)Postgresql: "+rs.getString("dpi")+", 2)Mysql: "+rs.getString("dpi")+"\n");
                    
                    Date fecha_postg = sdf.parse(post_fecha.toString(), new ParsePosition(0));//convierte de String a fecha 
                    Date fecha_mysql = sdf.parse(mysq_fecha.toString(), new ParsePosition(0));//convierte de String a fecha
                        //De mysql a Postgresql
                     if (fecha_postg.before(fecha_mysql)){//System.out.println("La fecha de Mysql es la Mas actual");
                     ac_postgres.actualizar_postgres(rsm.getString("dpi"),rsm.getString("nombre"),rsm.getString("apellido"),rsm.getString("direccion"),rsm.getString("tel_casa"),rsm.getString("tel_mobil"),rsm.getString("nom_contacto"),rsm.getString("t_nom_contacto"),fecha_mysql.toString());
                     }
                     else if(fecha_mysql .before(fecha_postg)){//System.out.println(" La Fecha de posgrest es la mas actual");
                     ac_mysql.actualizar_mysql(rs.getString("dpi"),rs.getString("nombre"),rs.getString("apellido"),rs.getString("direccion"),rs.getString("tel_casa"),rs.getString("tel_mobil"),rs.getString("nom_contacto"),rs.getString("t_nom_contacto"),post_fecha.toString());
                     }else{//System.out.println("Las fechas son iguales");
            }              
            }}}catch(Exception ex){}
        cin_mysql(); 
        cin_postgres();
    } 
    
    
    public void cin_mysql() throws SQLException
    {      /////////////////////////MYSQL///////////////////////////
            java.sql.Statement stmtm = cone_mysql();

          try{
          
          String datos=arbol.datos();//System.out.println(datos);
            // System.out.println("Datos Replicados: "+datos+"\n\n"+"Desiguales \nMysql ");
                ResultSet rsm=stmtm.executeQuery(sql_persona+"WHERE dpi not in("+datos+")");//mysql   
                while(rsm.next()){CharSequence mysq_fecha=rsm.getString("fecha").subSequence(0, 19);//System.out.println("Dpi: "+rsm.getString("dpi")+", Fecha: "+mysq_fecha);
                ac_postgres.insertar(rsm.getString("dpi"),rsm.getString("nombre"),rsm.getString("apellido"),rsm.getString("direccion"),rsm.getString("tel_casa"),rsm.getString("tel_mobil"),rsm.getString("nom_contacto"),rsm.getString("t_nom_contacto"),mysq_fecha.toString());
          
              }}catch(Exception ex){
              //System.out.println("No hay datos Replicaos en Mysql");
            ResultSet rsm=stmtm.executeQuery(sql_persona);//mysql
                while(rsm.next()){CharSequence mysq_fecha=rsm.getString("fecha").subSequence(0, 19);    
                ac_postgres.insertar(rsm.getString("dpi"),rsm.getString("nombre"),rsm.getString("apellido"),rsm.getString("direccion"),rsm.getString("tel_casa"),rsm.getString("tel_mobil"),rsm.getString("nom_contacto"),rsm.getString("t_nom_contacto"),mysq_fecha.toString());
                
            }}
    }
        
    
        public void cin_postgres() throws SQLException
    {       /////////////////////////POSTGRESQL///////////////////////
            java.sql.Statement stmt = cone_postges();
           // ac_mysql act_mysql = new ac_mysql();
        try{
            String datos=arbol.datos();System.out.println(datos);
            ResultSet rs=stmt.executeQuery(sql_persona+"where dpi not in("+datos+")");//postgresl
            while(rs.next()){CharSequence post_fecha=rs.getString("fecha").subSequence(0, 19);//postgesql 
 
            ac_mysql.insertar(rs.getString("dpi"),rs.getString("nombre"),rs.getString("apellido"),rs.getString("direccion"),rs.getString("tel_casa"),rs.getString("tel_mobil"),rs.getString("nom_contacto"),rs.getString("t_nom_contacto"),post_fecha.toString());
            }}catch(Exception ex){//System.out.println("\nHo hay datos Replicados en  Postgresql");
             ResultSet rs=stmt.executeQuery(sql_persona);//postgresl
            while(rs.next()){CharSequence post_fecha=rs.getString("fecha").subSequence(0, 19);//postgesql 
            ac_mysql.insertar(rs.getString("dpi"),rs.getString("nombre"),rs.getString("apellido"),rs.getString("direccion"),rs.getString("tel_casa"),rs.getString("tel_mobil"),rs.getString("nom_contacto"),rs.getString("t_nom_contacto"),post_fecha.toString());
         
            }}}
    
}
