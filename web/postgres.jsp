<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.lang.String"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/estilo.css">
        <link rel="stylesheet" href="css/db.css">
        <link rel="stylesheet" href="css/normalize.css">
        <script src="js/modernizr.custom.60251.js"></script>
        <script src="js/js.js"></script>
        <title>Postgresql</title>
    </head>
    <body>
    
        <div class="h">
            <nav>
                <ul>
                    <li><a href="inicio.jsp">Inicio</a></li>
                    <li><a href="mysql.jsp">Mysql</a></li>
                    
                        <div class="derecha">
                        <li class="verde">Postgresql</li>
                        <li><img src="img/db_post.png" class="in"></li>
                        <li class="fecha"></li>
                    
                </ul>
            </nav>
        </div>
    <br><br>
     <center>     
      <div class="contendor">
        <form action="in_postgres" method="post">
            <table border="0">
           <tr>
            <th>DPI</th>
            <td><input type="text" name="dpi" required="required"></td>
            <th>Nombre</th>
            <td><input type="text"  name="nombre" required="required"></td>
            <th>Apellidos</th>
            <td><input type="text" name="apellido" required="required"></td>
           </tr>
         
           <tr>
            <th>Direccion</th>
            <td><input type="text"  name="direccion" ></td>
             
            <th>Tel Casa</th>
            <td><input type="text" name="tel_casa"></td>
          
            <th>Tel Mobil</th>
            <td><input type="text"  name="tel_mobil"></td>
        
           </tr>
           <tr>
            <th>Nombre Contacto</th>
            <td><input type="text" name="nom_contacto" ></td>
      
            <th>N T Contacto</th>
            <td><input type="text" name="t_nom_contacto" required="required"></td>
            
            <td><input type="submit" value="Registrar"></td>
           </tr>
            </table>
            <br>
            
        </form>
          <br><br><br>
        
         <%
        DriverManager.registerDriver(new org.postgresql.Driver());
        String url = "jdbc:postgresql://localhost:5432/proyecto1";

Connection conexion = DriverManager.getConnection (url, "daju300", "wester");
Statement stmt = conexion.createStatement();
%>
        
<table border="1">
    <th>DPI</th>
    <th>NOMBRE</th>
    <th>APELLIDO</th>
    <th>DIRECCION</th>
    <th>TEL CASA</th>
    <th>TEL MOBIL</th>
    <th>NOM CONTACTO</th>
    <th>N T CONTACTO</th>
<%
ResultSet rs=stmt.executeQuery("select * from persona order by dpi");
while(rs.next()){
%>
<tr>
<td><form action="elim_postgres" method="post"><input type="text" name="eli" value="<%=rs.getString("dpi")%>"></td>
<td><%=rs.getString("nombre")%></td>
<td><%=rs.getString("apellido")%></td>
<td><%=rs.getString("direccion")%></td>
<td><%=rs.getString("tel_casa")%></td>
<td><%=rs.getString("tel_mobil")%></td>
<td><%=rs.getString("nom_contacto")%></td>
<td><%=rs.getString("t_nom_contacto")%></td>
<td><input type="submit" value="Eliminar"></form></td>
</tr>
<% } %></table>
</div>
    </center>
    </body>
    
    <footer></footer>
    </body>
</html>
