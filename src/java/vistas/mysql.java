package vistas;

import archivos.bitacora;
import archivos.sincro_archivos;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class mysql {
 
    private final String classfor = "com.mysql.jdbc.Driver";
    private final String url = "jdbc:mysql://localhost:3306/proyecto1";
    private final String usuario = "root";
    private final String clave = "";
    
    private String dpi;
    bitacora bt=new bitacora();
    sincro_archivos scr_arch= new sincro_archivos();
    String base="mysql";
    
    private Connection con = null;
    private PreparedStatement pr=null; 
    private ResultSet rs=null;
    String Secret_key = "9bddde1857cbd346a23f3ea8efd9c2deecb855c0a77a3ee5046cb0f2b3ef07e39c3b400880cc3654c875c0d025a6ba45202ce58ad3db8bf2c4cabf7195a43f10";
    
    
    public void insertar(String _dpi,String _nombre,String _apellido,String _direccion,String _tel_casa,String _tel_mobil,String _nom_contacto,String _n_tel_contacto) throws IOException{
       String sql = "insert into persona(dpi,nombre,apellido,direccion,tel_casa,tel_mobil,nom_contacto,t_nom_contacto) values(?,?,?,?,?,?,?,?)";
       try{

           Class.forName(classfor); 
           con= DriverManager.getConnection(url,usuario,clave);
           bt.insertar(_dpi, _nombre, _apellido, _direccion, _tel_casa, _tel_mobil, _nom_contacto, _nom_contacto, base);
           scr_arch.insertar_historial(_dpi, _nombre, _apellido, _direccion, _tel_casa, _tel_mobil, _nom_contacto, _n_tel_contacto, base);
           pr =con.prepareStatement(sql);
           pr.setString(1, _dpi);
           pr.setString(2, _nombre);
           pr.setString(3, _apellido);
           pr.setString(4, _direccion);
           pr.setString(5, _tel_casa);
           pr.setString(6, _tel_mobil);
           pr.setString(7, _nom_contacto);
           pr.setString(8, _n_tel_contacto);

           pr.executeUpdate();
       }catch(ClassNotFoundException | SQLException ev){
       }
    }
    
     public void eliminar(String _dpi) throws IOException{
       try{
     
     Class.forName(classfor);
     con= DriverManager.getConnection(url,usuario,clave);              
     PreparedStatement ps=con.prepareStatement("delete from persona where dpi=?");
     bt.eli_archivo(_dpi, base);
     ps.setString(1, _dpi);
     
     if (ps.execute()){
     throw new NullPointerException("no se pudo Eliminar");
           }
       }catch(ClassNotFoundException | SQLException | NullPointerException e){       
       }
   }
     
     public void borra_todo(){
       try{
     String tabla="persona";
     Class.forName(classfor);
     con= DriverManager.getConnection(url,usuario,clave);
     PreparedStatement ps=con.prepareStatement("truncate ?");
     ps.setString(1, tabla);
     
     if (ps.execute()){
     throw new NullPointerException("no se pudo Eliminar");
           }
       }catch(ClassNotFoundException | SQLException | NullPointerException e){       
       }
   }

     public void registrar_usuario(String _user,String _pass,String _email,String _telefono){
     
          String sql = "insert into usuario(Usuario,Contraseña,correo,telefono) values(?,?,?,?)";
       try{          
           String d = cifrar_claves.getStringMessageDigest(_pass, cifrar_claves.SHA512);
           
           System.out.println(_user+_pass+_email+_telefono);
           Class.forName(classfor); 
           con= DriverManager.getConnection(url,usuario,clave);
           
           pr =con.prepareStatement(sql);
           pr.setString(1, _user);
           pr.setString(2, d);
           pr.setString(3, _email);
           pr.setString(4, _telefono);

           pr.executeUpdate();
       }catch(ClassNotFoundException | SQLException ev){
       }}
     
     public void actualizar_mysql(String _dpi,String _nombre,String _direccion,String _apellido,String _tel_casa,String _tel_mobil,String _nom_contacto,String _n_tel_contacto){
       try{
           java.util.Date _fecha = new Date();
       String sql="UPDATE persona SET nombre=?, apellido=?, direccion=?, tel_casa=?, tel_mobil=?, nom_contacto=?, t_nom_contacto=? WHERE dpi=?"; 
           Class.forName(classfor); 
           con= DriverManager.getConnection(url,usuario,clave);
           pr =con.prepareStatement(sql);
           pr.setString(1, _nombre);
           pr.setString(2, _direccion);
           pr.setString(3, _apellido);
           pr.setString(4, _tel_casa);
           pr.setString(5, _tel_mobil);
           pr.setString(6, _nom_contacto);
           pr.setString(7, _n_tel_contacto);
           pr.setString(8, _dpi);
         
           pr.executeUpdate();
           
       }catch(ClassNotFoundException | SQLException ev){}}
    
}
