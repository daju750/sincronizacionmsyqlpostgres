package vistas;

import java.sql.DriverManager;
import java.sql.SQLException;

public class conexion_usuario {
    
  public static final String USERNAME = "root";
  public static final String PASSWORD = "";
  public static final String HOST = "localhost";
  public static final String PORT = "3306";
  public static final String DATABASE = "proyecto1";
  public static final String CLASSNAME = "com.mysql.jdbc.Driver";
  public static final String URL = "jdbc:mysql://"+ HOST +":"+PORT+"/"+DATABASE;
  
  public java.sql.Connection con;
  
  public conexion_usuario()
      {try{Class.forName(CLASSNAME);
      con = DriverManager.getConnection(URL, USERNAME, PASSWORD);
      }catch (ClassNotFoundException | SQLException e){}}  
}
