package sincronizacion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ac_postgres {
    
    private Connection con = null;
    private PreparedStatement pr=null; 
    
        String classfor = "org.postgresql.Driver";
        String url = "jdbc:postgresql://localhost:5432/proyecto1";
        String usuario = "daju300";
        String clave = "wester";
        String base="postgres";
    
     public void actualizar_postgres(String _dpi,String _nombre,String _direccion,String _apellido,String _tel_casa,String _tel_mobil,String _nom_contacto,String _n_tel_contacto,String _fecha){
       try{
       String sql="UPDATE persona SET nombre=?, apellido=?, direccion=?, tel_casa=?, tel_mobil=?, nom_contacto=?, t_nom_contacto=?, fecha='"+_fecha+"' WHERE dpi=?"; 
           Class.forName(classfor); 
           con= DriverManager.getConnection(url,usuario,clave);
           pr =con.prepareStatement(sql);
           pr.setString(1, _nombre);
           pr.setString(2, _direccion);
           pr.setString(3, _apellido);
           pr.setString(4, _tel_casa);
           pr.setString(5, _tel_mobil);
           pr.setString(6, _nom_contacto);
           pr.setString(7, _n_tel_contacto);
           pr.setString(8, _dpi);
         
           pr.executeUpdate();
           
       }catch(ClassNotFoundException | SQLException ev){//System.out.println("Error sql");System.out.println(_dpi+", "+_fecha);
       }
    }

     public void insertar(String _dpi,String _nombre,String _direccion,String _apellido,String _tel_casa,String _tel_mobil,String _nom_contacto,String _n_tel_contacto,String _fecha){
       String sql = "insert into persona(dpi,nombre,apellido,direccion,tel_casa,tel_mobil,nom_contacto,t_nom_contacto,fecha) values(?,?,?,?,?,?,?,?,'"+_fecha+"')";
       try{         
           Class.forName(classfor); 
           con= DriverManager.getConnection(url,usuario,clave);
           
           pr =con.prepareStatement(sql);
           pr.setString(1, _dpi);
           pr.setString(2, _nombre);
           pr.setString(3, _direccion);
           pr.setString(4, _apellido);
           pr.setString(5, _tel_casa);
           pr.setString(6, _tel_mobil);
           pr.setString(7, _nom_contacto);
           pr.setString(8, _n_tel_contacto);

           pr.executeUpdate();
           
       }catch(ClassNotFoundException | SQLException ev){//System.out.println("No se pudo ejecutar el query");
       }}
    
}
