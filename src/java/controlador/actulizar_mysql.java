package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import vistas.mysql;

public class actulizar_mysql extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            mysql cons= new mysql();
            String dpi =request.getParameter("dpi");
            String nombre =request.getParameter("nombre");
            String direccion =request.getParameter("direccion");
            String apellido =request.getParameter("apellido");
            String tel_casa =request.getParameter("tel_casa");
            String tel_mobil =request.getParameter("tel_mobil");
            String nom_contacto =request.getParameter("nom_contacto");
            String t_nom_contacto =request.getParameter("t_nom_contacto");

            //cons.insertar(dpi,nombre,apellido,direccion,tel_casa,tel_mobil,nom_contacto,t_nom_contacto);
            cons.actualizar_mysql(dpi, nombre, direccion, apellido, tel_casa, tel_mobil, t_nom_contacto, t_nom_contacto);
            response.sendRedirect("ac_mysql.jsp");
            
        } finally{
        out.close();}
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
