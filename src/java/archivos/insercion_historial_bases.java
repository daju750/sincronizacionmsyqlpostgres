/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package archivos;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Random;

/**
 *
 * @author daju
 */
public class insercion_historial_bases {

     public void escribir_txt(String cadena){
     File f=new File("/home/daju/NetBeansProjects/WebApplication2/prueba.txt");
    try {
        FileWriter fw=new FileWriter(f);
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
         fw.write(cadena);
            fw.close();
        }
        catch (IOException e) {
        }   
}
    
    public void insert_historial_mysql(String _cuerpo) throws IOException{
        
        String classfor = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/proyecto1";
        String usuario = "root";
        String clave = "";
        Connection con = null;
        PreparedStatement pr=null;
        String sql = "INSERT INTO historial(cuerpo) VALUES(?)";
       try{Class.forName(classfor); 
           con= DriverManager.getConnection(url,usuario,clave);          
           pr =con.prepareStatement(sql);
           pr.setString(1, _cuerpo);
           pr.executeUpdate();
       }catch(ClassNotFoundException | SQLException ev){}}
   
    public void insert_historial_posgresql(String _cuerpo){
    
        String classfor = "org.postgresql.Driver";
        String url = "jdbc:postgresql://localhost:5432/proyecto1";
        String usuario = "daju300";
        String clave = "wester"; 
        Connection con = null;
        PreparedStatement pr=null;
    
        String sql = "insert into historial(cuerpo) values('"+_cuerpo+"')";
       try{Class.forName(classfor); 
           con= DriverManager.getConnection(url,usuario,clave);
           pr =con.prepareStatement(sql);
           pr.executeUpdate();
       }catch(ClassNotFoundException | SQLException ev){System.out.println("Eror Sql");}
    }
    
    public void after_sinc(){
     File f=new File("/home/daju/NetBeansProjects/Proyecto1_Sincronizacion/bitacora.txt");
    try {
        FileWriter fw=new FileWriter(f);
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        String texto="";
         fw.write("\n"+texto+"\r\n"); 
            fw.close();
        }catch (IOException ex){}  
    }
    
    public void insecion_historial() throws IOException{
        insert_historial_posgresql(leer_archivo_txt());;
        insert_historial_mysql(leer_archivo_txt());
        after_sinc();}
    
    
    public void escribir_binario(String cadena){
    File f=new File("/home/daju/NetBeansProjects/Proyecto1_Sincronizacion/archivo.bin"); 
    Random r=new Random();double d=18.76353;
    try{FileOutputStream fis=new FileOutputStream(f);
        DataOutputStream dos=new DataOutputStream(fis);
       for (int i=0;i<234;i++){ //Se repite 233 veces
        dos.writeDouble(r.nextDouble());//No aleatorio
        }dos.close();}
        catch(FileNotFoundException e){System.out.println("No se encontro el archivo");}
    catch(IOException e){System.out.println("Error al escribir");}}
    
    public String leer_archivo_txt(){
    String variable="";
    File f=new File("/home/daju/NetBeansProjects/Proyecto1_Sincronizacion/bitacora.txt");
    try{
        int t=0;FileReader fr=new FileReader(f);BufferedReader br=new BufferedReader(fr);
        String s="";br.readLine();
        while(s!=null){variable+=s+"\n";
        s=br.readLine();t++;}
    }catch (IOException e){}
    return variable;}
    
    public void leer_binario() throws IOException{
        File f=new File("/home/daju/NetBeansProjects/Proyecto1_Sincronizacion/bitacora.txt");
        boolean finArchivo=false;//Para provocar bucle infinito
        try{FileInputStream fis=new FileInputStream(f);DataInputStream dis=new DataInputStream(fis);
        double d;while (!finArchivo){d=dis.readDouble();escribir_txt(null);
        }dis.close();}catch(EOFException e){finArchivo=true;}catch(FileNotFoundException e){}}
    
}
