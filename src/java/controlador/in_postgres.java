package controlador;

import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.System.out;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import vistas.postgres;

public class in_postgres extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
        postgres cons= new postgres();
        
            String dpi =request.getParameter("dpi");
            String nombre =request.getParameter("nombre");
            String apellido =request.getParameter("apellido");
            String direccion =request.getParameter("direccion");
            String tel_casa =request.getParameter("tel_casa");
            String tel_mobil =request.getParameter("tel_mobil");
            String nom_contacto =request.getParameter("nom_contacto");
            String t_nom_contacto =request.getParameter("t_nom_contacto");
  
            cons.insertar(dpi,nombre,direccion,apellido,tel_casa,tel_mobil,nom_contacto,t_nom_contacto);
            
            response.sendRedirect("postgres.jsp");
        }finally{
        out.close();}
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
    
}
