<%-- 
    Document   : sincronizacion
    Created on : 04-sep-2015, 17:37:14
    Author     : daju
--%>

<%@page import="java.lang.String"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.DriverManager"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="css/estilo.css">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/db.css">
        <script src="js/js.js"></script>
        <script src="js/modernizr.custom.60251.js"></script>
        <title>Cincronizacion</title>
    </head>
    <body>
     <div class="h">
            <nav>
                <ul>
                    <li><a href="inicio.jsp">Inicio</a></li>
                    <li><a href="postgres.jsp">Postgresql</a></li>
                    <li><a href="mysql.jsp">Mysql</a></li>
                        <div class="derecha">
                        
                        <li><img src="img/open.jpg" class="in"></li>
                        <li class="fecha"><script>fecha();</script></li>
                    
                </ul>
            </nav>
        </div>
          <br><br><br>  
 
   <center><div class="contendor">         
   <h2>Postresql</h2>
             
         <%
        DriverManager.registerDriver(new org.postgresql.Driver());
        String url = "jdbc:postgresql://localhost:5432/proyecto1";

        Connection conexion = DriverManager.getConnection (url, "daju300", "wester");
        Statement stmt = conexion.createStatement();
        %>
        
         <%
        DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        String urlm = "jdbc:mysql://localhost:3306/proyecto1";

        Connection conexionm = DriverManager.getConnection (urlm, "root", "");
        Statement stmtm = conexionm.createStatement();
        %>

    <center>
<table border="1">
    <th>DPI</th>
    <th>NOMBRE</th>
    <th>APELLIDO</th>
    <th>DIRECCION</th>
    <th>TEL CASA</th>
    <th>TEL MOBIL</th>
    <th>NOM CONTACTO</th>
    <th>N T CONTACTO</th>
    <th>Fecha</th>
<%
ResultSet rs=stmt.executeQuery("select * from persona order by dpi");
while(rs.next()){
%>
<tr>
<td><%=rs.getString("dpi")%></td>
<td><%=rs.getString("nombre")%></td>
<td><%=rs.getString("apellido")%></td>
<td><%=rs.getString("direccion")%></td>
<td><%=rs.getString("tel_casa")%></td>
<td><%=rs.getString("tel_mobil")%></td>
<td><%=rs.getString("nom_contacto")%></td>
<td><%=rs.getString("t_nom_contacto")%></td>
<td><%=rs.getString("fecha").subSequence(0, 19)%></td>
</tr>
<% } %></table>
<h2>Mysql</h2>
<table border="1">
    <th>DPI</th>
    <th>NOMBRE</th>
    <th>APELLIDO</th>
    <th>DIRECCION</th>
    <th>TEL CASA</th>
    <th>TEL MOBIL</th>
    <th>NOM CONTACTO</th>
    <th>N T CONTACTO</th>
    <th>Fecha</th>
<% 
ResultSet rsm=stmtm.executeQuery("select * from persona order by dpi");
while(rsm.next()){
%>
<tr>
<td><%=rsm.getString("dpi")%></td>
<td><%=rsm.getString("nombre")%></td>
<td><%=rsm.getString("apellido")%></td>
<td><%=rsm.getString("direccion")%></td>
<td><%=rsm.getString("tel_casa")%></td>
<td><%=rsm.getString("tel_mobil")%></td>
<td><%=rsm.getString("nom_contacto")%></td>
<td><%=rsm.getString("t_nom_contacto")%></td>
<td><%=rsm.getString("fecha").subSequence(0, 19)%></td>
</tr>
<% } %>
</table> 
<br><br><br>

<table>
    <tr>      
<td><form action="sincronizar" method="post"><input type="submit" value="Sincronizacion por Bases"></form></td>
<td><input type="submit" value="?" onclick="ayuda_sincro();"></td>
    </tr>
    <tr>      
<td><br><input type="submit" value="Sincronizacion en Archivo"></td>
<td><br><input type="submit" value="?" onclick="ayuda_archivo();"></td>
    </tr>
</table>
<br><br>
    </center>
    </div>
        
    </body>
</html>
