/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package archivos;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author daju
 */
public class cifrado {
    
    public static String MD2 = "MD2";
	    public static String MD5 = "MD5";
	    public static String SHA1 = "SHA-1";
	    public static String SHA256 = "SHA-256";
	    public static String SHA384 = "SHA-384";
	    public static String SHA512 = "SHA-512";
    
public static String TxT,Seed;
public static String Charset = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKMNOPQRSTUVWXYZ,+-:*.() ";
        
public void Vigenere(String TxT,String Seed){
this.Seed = Desn(Seed);
this.TxT = TxT;
}
        
    public String Cifrar(){
	String Cifrado = "",a=TxT.toLowerCase();
            for(int n = 0,c=0;n<TxT.length();n++,c = (c+1)%Seed.length()){
                if(Charset.indexOf(a.charAt(n))!=-1){
                    int tmp = (Charset.indexOf(a.charAt(n))+Charset.indexOf(Seed.charAt(c)))%Charset.length();
                    Cifrado+=(Charset.indexOf(TxT.charAt(n))!=-1)?Charset.charAt(tmp):String.valueOf(Charset.charAt(tmp)).toUpperCase();
                }else{
            c-=1;
            Cifrado+=TxT.charAt(n);
	}}
	return Cifrado;}
        
	public String DesCifrar(){
            String DesCifrado = "";
            String a = TxT.toLowerCase();
            for(int n = 0,c=0;n<a.length();n++,c=(c+1)%Seed.length()){
                if(Charset.indexOf(a.charAt(n))!=-1){
                    int tmp = (Charset.indexOf(a.charAt(n))-Charset.indexOf(Seed.charAt(c)));
                    tmp = (tmp<0)?(tmp+Charset.length()):tmp;
                    DesCifrado+=(Charset.indexOf(TxT.charAt(n))!=-1)?Charset.charAt(tmp):String.valueOf(Charset.charAt(tmp)).toUpperCase();
                }else{
                    c-=1;
                    DesCifrado+=TxT.charAt(n);
                }}
	return DesCifrado;
	}
        
	public String Desn(String a){
	String b = "";
            for(int n = 0;n<a.length();n++)
            {if((Charset.indexOf(a.charAt(n))!=-1)||(Charset.indexOf(String.valueOf(a.charAt(n)).toLowerCase())!=-1))
             b+=a.charAt(n);}
	return b;
	}
        
	 
	    private static String toHexadecimal(byte[] digest){
	        String hash = "";
	        for(byte aux : digest) {
	            int b = aux & 0xff;
	            if (Integer.toHexString(b).length() == 1) hash += "0";
	            hash += Integer.toHexString(b);
	        }
	        return hash;
	    }
	    public static String getStringMessageDigest(String message, String algorithm){
	        byte[] digest = null;
	        byte[] buffer = message.getBytes();
	        try {
	            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
	            messageDigest.reset();
	            messageDigest.update(buffer);
	            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException ex) {
	            System.out.println("Error creando Digest");
	        }
	        return toHexadecimal(digest);
	    }
    
}
