package vistas;

import archivos.bitacora;
import archivos.sincro_archivos;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class postgres {
    
    String classfor = "org.postgresql.Driver";
     String url = "jdbc:postgresql://localhost:5432/proyecto1";
     String usuario = "daju300";
     String clave = "wester";
     String base="postgres";
     bitacora bt=new bitacora();
     sincro_archivos scr_arch= new sincro_archivos(); 
    private Connection con = null;
    private PreparedStatement pr=null; 
    private ResultSet rs=null;
    
    public void insertar(String _dpi,String _nombre,String _direccion,String _apellido,String _tel_casa,String _tel_mobil,String _nom_contacto,String _n_tel_contacto) throws IOException{
       String sql = "insert into persona(dpi,nombre,apellido,direccion,tel_casa,tel_mobil,nom_contacto,t_nom_contacto) values(?,?,?,?,?,?,?,?)";
       try{
           java.util.Date fecha = new Date();

           con= DriverManager.getConnection(url,usuario,clave);
           bt.insertar(_dpi, _nombre, _apellido, _direccion, _tel_casa, _tel_mobil, _nom_contacto, _nom_contacto,base);
           scr_arch.insertar_historial(_dpi, _nombre, _apellido, _direccion, _tel_casa, _tel_mobil, _nom_contacto, _n_tel_contacto, base);
           pr =con.prepareStatement(sql);
           pr.setString(1, _dpi);
           pr.setString(2, _nombre);
           pr.setString(3, _apellido);
           pr.setString(4, _direccion);
           pr.setString(5, _tel_casa);
           pr.setString(6, _tel_mobil);
           pr.setString(7, _nom_contacto);
           pr.setString(8, _n_tel_contacto);
           //pr.setString(9, fecha.toString());
        /* */  pr.executeUpdate();
           
       }catch(SQLException ev){}
    }
    
    public void eliminar(String _dpi){
       try{
     bt.eli_archivo(_dpi, base);
     Class.forName(classfor);
     con= DriverManager.getConnection(url,usuario,clave);              
     PreparedStatement ps=con.prepareStatement("delete from persona where dpi=?");
     
     ps.setString(1, _dpi);
     
     if (ps.execute()){
     throw new NullPointerException("no se pudo Eliminar");}
       }catch(IOException | ClassNotFoundException | NullPointerException | SQLException e){}}
    
}