package sincronizacion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ac_mysql {
    
    String classfor = "com.mysql.jdbc.Driver";
    String url = "jdbc:mysql://localhost:3306/proyecto1";
    String usuario = "root";
    String clave = "";
     
    private Connection con = null;
    private PreparedStatement pr=null;
    
    
    public void actualizar_mysql(String _dpi,String _nombre,String _direccion,String _apellido,String _tel_casa,String _tel_mobil,String _nom_contacto,String _n_tel_contacto,String _fecha){
       try{
       String sql="UPDATE persona SET nombre=?, apellido=?, direccion=?, tel_casa=?, tel_mobil=?, nom_contacto=?, t_nom_contacto=?, fecha='"+_fecha+"' WHERE dpi=?"; 
           Class.forName(classfor); 
           con= DriverManager.getConnection(url,usuario,clave);
           pr =con.prepareStatement(sql);
           pr.setString(1, _nombre);
           pr.setString(2, _direccion);
           pr.setString(3, _apellido);
           pr.setString(4, _tel_casa);
           pr.setString(5, _tel_mobil);
           pr.setString(6, _nom_contacto);
           pr.setString(7, _n_tel_contacto);
           pr.setString(8, _dpi);
         
           pr.executeUpdate();
           
       }catch(ClassNotFoundException | SQLException ev){}}
    
        public void insertar(String _dpi,String _nombre,String _direccion,String _apellido,String _tel_casa,String _tel_mobil,String _nom_contacto,String _n_tel_contacto,String _fecha){
       String sql = "insert into persona(dpi,nombre,apellido,direccion,tel_casa,tel_mobil,nom_contacto,t_nom_contacto,fecha) values(?,?,?,?,?,?,?,?,'"+_fecha+"')";
       try{         
           Class.forName(classfor); 
           con= DriverManager.getConnection(url,usuario,clave);
           
           pr =con.prepareStatement(sql);
           pr.setString(1, _dpi);
           pr.setString(2, _nombre);
           pr.setString(3, _direccion);
           pr.setString(4, _apellido);
           pr.setString(5, _tel_casa);
           pr.setString(6, _tel_mobil);
           pr.setString(7, _nom_contacto);
           pr.setString(8, _n_tel_contacto);

           pr.executeUpdate();
           
       }catch(Exception ev){}}
    
}
